import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 一旦決め打ち
    nextTaskId: 3,
    nextLabelId: 4,
    filterLabelId: null,
    tasks: [
      {
        id: 1,
        name: 'hoge',
        labelIds: [1, 2],
        done: false
      },
      {
        id: 2,
        name: 'fuga',
        labelIds: [1, 3],
        done: true
      },
    ],
    labels: [
      {
        id: 1,
        name: 'food'
      },
      {
        id: 2,
        name: 'drink'
      },
      {
        id: 3,
        name: 'book'
      },
    ]
  },
  getters: {
    filterdTasks(state) {
      // ラベルが洗濯されていなければそのままの一覧を返す
      if (!state.filterLabelId) {
        return state.tasks
      }
      console.log('filterうごいてる')

      // 洗濯されているラベルでフィルタリングする
      return state.tasks.filter(task => task.labelIds.indexOf(state.filterLabelId) >= 0)
    }
  },
  mutations: {
    // add task
    addTask(state, { name, labelIds }) {
      if (!name) return false

      state.tasks.push({
        id: state.nextTaskId,
        name,
        labelIds,
        done: false,
      })
      state.nextTaskId++
    },
    // add label
    addLabel(state, { name }) {
      if (!name) return false

      state.labels.push({
        id: state.nextLabelId,
        name,
      })
      state.nextLabelId++
    },
    // toggle task status done/undone
    toggleTaskStatus(state, { id }) {
      const filterd = state.tasks.filter(task => task.id === id)
      filterd.done = !filterd.done
    },


    // フィルタリング対象のラベルを変更する
    changeFilter(state, { filterLabelId }) {
      state.filterLabelId = filterLabelId
    },

    // ステートを復元する
    restore(state, { tasks, labels, nextTaskId, nextLabelId }) {
      state.tasks = tasks
      state.labels = labels
      state.nextTaskId = nextTaskId
      state.nextLabelId = nextLabelId
    }
  },
  actions: {
    // save state to local storage
    save({ state }) {
      const data = {
        tasks: state.tasks,
        labels: state.labels,
        nextTaskId: state.nextTaskId,
        nextLabelId: state.nextLabelId,
      }
      localStorage.setItem('task-app-data', JSON.stringify(data))
    },

    // restore state from local storage
    restore({ commit }) {
      const data = localStorage.getItem('task-app-data')
      if (data) {
        commit('restore', JSON.parse(data))
      }
    }
  }
})
